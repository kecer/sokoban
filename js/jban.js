Array.prototype.lookFor = (
	function (val){
		for(var i=0;i<this.length;i++){
			if(this[i][0]==val[0] && this[i][1]==val[1]){
				return i;
			}
		}
		return -1;
	}
);

$(function(){
	var tile = {"width":20,"height":20};

	// setting positions in source image, assigning appropriate sign
	// @ - sokoban, + - sokoban on target spot
	// $ - box, * - box on target spot
	// . - target spot
	// # - wall
	// - - empty

	var icon = {"@":[20,0],"+":[20,20],"$":[40,0],"*":[40,20],".":[0,20],"#":[0,40]};
	var empty = "-";

	//var map = "----#####----------|----#---#----------|----#$--#----------|--###--$##---------|--#--$-$-#---------|###-#-##-#---######|#---#-##-#####--..#|#-$--$----------..#|#####-###-#@##--..#|----#-----#########|----#######--------";
	var map = $("textarea").text();
	var keys = {"37":"left","38":"up","39":"right","40":"down"};
	// counting width and height from map
	var dim = dimensions(map);

	var moves = 0;

	// loading image (contains all icons - positions are in var icon)
	var img = new Image();
	img.src = 'img/full.png';

	// creating basic objects
	var sokoban = {"x":0,"y":0};
	var targets = new Array();
	var boxes   = new Array();
	var walls   = new Array();

	// computing dimensions of desired canvas
	function dimensions(){
		var s = map.split("|");
		return {"width":tile["width"]*s[0].length,
				"height":tile["height"]*s.length}
	}

	// creating canvas, setting w&h, bgcolor and border
	function generate_canvas(){
		// unfortunately, the following commented part won't work
		// .ready() won't wait for dynamically created elements
		// and .delegate/.on doesn't seem to work for me.
		// we have to generate canvas with php

		//$("body").append($("canvas",{
		//	"id":"game",
		//	"width":dim["width"],
		//	"height":dim["height"]
		//}));
		var c = $("canvas#game");

		window.canvas = c[0].getContext("2d");

		clean_up();
		canvas.stroke.Style="black";
		canvas.strokeRect(0,0,dim["width"],dim["height"]);
	}
	function clean_up(){
		canvas.fillStyle="#c5c5c5";
		canvas.fillRect(0,0,dim["width"],dim["height"]);
	}

	function draw(x,y,ipos){
		canvas.drawImage(img, ipos[0], ipos[1], 20, 20, x, y, 20, 20);
	}
	function init_draw(){
		// canvas init
		generate_canvas();
		var x = 0;
		var y = 0;
		var pos = {"x":0,"y":0};
		var m = map.split("");
		for(var i=0; i<m.length; i++){
			var area = m[i];
			if(area=="|"){
				y+=tile["width"];
				x=0;
				pos.y+=1;
				pos.x=0;
				continue;
			}
			// checking, if we aren't beyond limit
			if(x==dim["width"]){
				alert("All lines aren't the same length!");
				break;
			}
			// draw non-empty fields
			if(area!="-"){
				draw(x,y,icon[area]);

				if(area=="@"){
					sokoban.x = pos.x;
					sokoban.y = pos.y;
				}
				else if(area=="+"){
					targets.push([pos.x,pos.y]);
				}
				else if(area=="$"){
					boxes.push([pos.x,pos.y]);
				}
				else if(area=="*"){
					boxes.push([pos.x,pos.y]);
					targets.push([pos.x,pos.y]);
				}
				else if(area=="#"){
					walls.push([pos.x,pos.y]);
				}
				else if(area=="."){
					targets.push([pos.x,pos.y]);
				}
				else{
					console.log("Unknown character '"+area+"'. Assuming empty.");
				}
			}
			x+=tile["width"];
			pos.x+=1;
		}
	}

	function move(direction){
		var x = sokoban.x;
		var y = sokoban.y;
		var next = {"x":x,"y":y};

		if(direction=="left")      {x -= 1; next.x -= 2;}
		else if(direction=="right"){x += 1; next.x += 2;}
		else if(direction=="up")   {y -= 1; next.y -= 2;}
		else if(direction=="down") {y += 1; next.y += 2;}
		else {return -1}; //undefined behaviour

		if(walls.lookFor([x,y])>-1){
			// we can't move that way
			return -1;
		}
		if(boxes.lookFor([x,y])>-1){
			// box in our way, can we push it?
			if( boxes.lookFor([next.x,next.y])>-1 || walls.lookFor([next.x,next.y])>-1){
				// we can't
				return -1;
			}
			// we can, so lets move the box
			var i = boxes.lookFor([x,y]);
			boxes[i] = [next.x,next.y];
		}

		// we didnt die yet, therefore we can move
		var f = [x,y];
		sokoban.x = x;
		sokoban.y = y;

		moves++;

		// redraw screen
		redraw();
		check();
	}

	function redraw(){
		clean_up();
		var items = [walls,targets,boxes];
		for(var i=0; i<walls.length; i++){
			var x = walls[i][0]*tile["width"];
			var y = walls[i][1]*tile["height"];
			draw(x,y,icon["#"]);
		}
		for(var i=0; i<targets.length; i++){
			var x = targets[i][0]*tile["width"];
			var y = targets[i][1]*tile["height"];
			draw(x,y,icon["."]);
		}
		for(var i=0; i<boxes.length; i++){
			var x = boxes[i][0]*tile["width"];
			var y = boxes[i][1]*tile["height"];
			var symbol = "$";
			if(targets.lookFor(boxes[i])>-1){
				symbol = "*";
			}
			draw(x,y,icon[symbol]);
		}
		if(targets.lookFor([sokoban.x,sokoban.y])>-1){
			draw(sokoban.x*tile["width"],sokoban.y*tile["height"],icon["+"]);
		}
		else{
			draw(sokoban.x*tile["width"],sokoban.y*tile["height"],icon["@"]);
		}
	}
	function check(){
		for(var i=0;i<boxes.length;i++){
			if(targets.lookFor(boxes[i])==-1){
				//not yet done
				return -1;
			}
		}
		//done!
		alert("Good job! Moves: "+moves);
	}

	$(img).ready(init_draw());

	$(document).keydown(function(e){
		var key = e.which;
		move(keys[key]);
	})
})
