<?php
    $lvl = isset($_POST["text"])?$_POST["text"]:"----#####----------|----#---#----------|----#$--#----------|--###--$##---------|--#--$-$-#---------|###-#-##-#---######|#---#-##-#####--..#|#-$--$----------..#|#####-###-#@##--..#|----#-----#########|----#######--------";
	/* just some parsing for compatibility */
    $lvl = str_replace("\n","|",$lvl);
    $lvl = str_replace(" ","",$lvl);
    $lvl = str_replace("\r","",$lvl);
    $lvl = str_replace("||","|",$lvl);
    $x = explode("|",$lvl);
    $w = strlen($x[0])*20;
    $h = count($x)*20;
?>
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>jBan</title>
        <meta charset="UTF-8"/>
        <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
        <script src="js/jban.js"></script>
        <link rel="stylesheet" href="css/main.css"/>
    </head>
    <body>
    <canvas id="game" width=<?=$w?> height=<?=$h?>>
        Your browser doesn't support canvas :( Consider updating it.
    </canvas>

    <form method="POST">
        <textarea name="text" cols=80 rows=10><?=$lvl?></textarea><br/>
        <input type="submit" value="Start">
    </form>

    </body>
</html>
